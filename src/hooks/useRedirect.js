import { useSelector } from "react-redux";

function useRedirect() {
    const userName = useSelector(state => state.user);
    if (userName === null) window.location.assign("/startup");
}

export default useRedirect;