import { useMemo, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { setUser, setLog } from "../redux/user";
import { setStorage, getStorage } from "../utils/storage";
import {useHistory} from "react-router-dom";


function useUser() {
    const dispatch = useDispatch();
    const userName = useSelector(state => state.user);
    const logList = useSelector(state => state.logList);
    const isLoggedIn = useMemo(() => userName !== null, [userName]);
    const history = useHistory();   

    useEffect(() => {
        if (isLoggedIn) return;
        const user = getStorage("user");
        if (user) {
            console.log("Logging in as ", user.user);
            dispatch(setUser(user.user))
        }
        // eslint-disable-next-line
    }, [isLoggedIn])

    function logIn(name) {
        setUserInStorage(name);
        dispatch(setUser(name));
    }

    function setUserInStorage(name) {
        setStorage("user", {user: name});
    }

    function setLogSearch(log) {
        const {history} = getStorage("history");
        console.log(history);
        if(history) {
            if (history.length === 10) {
                history.shift();
            }
            history.push(log);
            setStorage("history", {history: history});
        } else {
            setStorage("history", {history: [log]});
        }
        dispatch(setLog(history));
    }

    function toTranslator() {
        history.push("/translator");
    }

    function toProfile() {
        history.push("/profile");
    }

    function logOut() {
        clearStorage();
        dispatch(setUser(null));
        window.location.assign("/startup");
    }

    function clearStorage() {
        localStorage.clear();
    }

    return {userName, logList, isLoggedIn, logIn, logOut, toProfile, toTranslator, setLogSearch}
}

export default useUser;