import { useState } from "react";
import { useDispatch } from "react-redux";
import { setUser } from "../redux/user";

function useLogin() {
    const [loggedIn, setLoggedIn] = useState(false);
    const dispatch = useDispatch();

    const gotUser = true;
    let userName = "";

    if (gotUser) {
        dispatch(setUser(userName))
        setLoggedIn(true);
    }

    return loggedIn;
}

export default useLogin;