import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import StartupPage from "./components/StartupPage";
import TranslationPage from "./components/TranslationPage";
import ProfilePage from "./components/ProfilePage";
import Header from "./components/Header";

function App() {
  return (
      <BrowserRouter>
        <div className="App">
          <Header/>
          <Switch>
            <Route exact path="/">
              <Redirect to="/startup"/>
            </Route>
            <Route path="/startup" component={StartupPage}/>
            <Route path="/translator" component={TranslationPage}/>
            <Route path="/profile" component={ProfilePage}/>
            <Route component={Error}></Route>
          </Switch>
        </div>
      </BrowserRouter>
  );
}

export default App;
