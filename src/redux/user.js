import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
    name: "user",
    initialState: {
        user: null,
        logList: []
    },
    reducers: {
        setUser(state, action) {
            state.user = action.payload;
        },
        setLog(state, action) {
            state.logList = action.payload;
        }
    }
});

const { actions, reducer } = userSlice;
export const { setUser, setLog } = actions;

export default reducer;