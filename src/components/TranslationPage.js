import React, {useState} from "react";
import useRedirect from "../hooks/useRedirect";
import useUser from "../hooks/useUser";

function TranslationPage() {
    const [texted, setTexted] = useState(""); //text as written
    const [translated, setTranslated] = useState([]); //text without spaces
    const {setLogSearch} = useUser();
    useRedirect();

    function onInputChange(e) {
        let value = e.target.value;
        value = value.replace(/([^A-Za-z ])/gi, "")
        setTexted(value);
    }   

    function handleTranslated() {
        setTranslated(texted.replace(/ /g, "").split(""))
        saveLog();
        setTexted("");
    }

    function saveLog() {
        setLogSearch(texted);
    }

    function onKeyUp(e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            handleTranslated();
        }
    }

    function onSubmit(e) {
        handleTranslated();
    }
    
    function Translated({translated}) {
        return (
            <div id="translated">
                {translated.map((char, index) => 
                    <img key={char + index} src={process.env.PUBLIC_URL +
                        `images/${char}.png`} alt={char + " letter"}/>
                )}
            </div>
        )
    }

    return (
        <section>
            <div id="transSearch">
                <input value={texted} id="textInput" maxLength="40"
                    placeholder="What do you want to translate?"
                    onChange={onInputChange} onKeyUp={onKeyUp}/>
                <button id="submitButt" onClick={onSubmit}>Submit</button>

            </div>
            <div id="translatedWrapper">
                <Translated translated={translated}/>
            </div>
        </section>
    );
}

export default TranslationPage;