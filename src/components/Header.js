import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import useUser from "../hooks/useUser";
import {useHistory, useLocation} from "react-router-dom";

function LoggedInHeader(props) {
    const { logOut, toProfile, toTranslator, userName } = useUser();
    const history = useHistory();
    const location = useLocation(); 

    return (
        <>
            <img className="smallLogo"
            src={process.env.PUBLIC_URL + 'images/Logo.png'} 
            alt="Happy robot logo"
            onClick={() => history.push("/")}/>
            <h2>Lost in Translation</h2>
            <button id="route" onClick={() => 
                (location.pathname === "/translator")
                    ?toProfile():toTranslator()}>
                {(location.pathname === "/translator")?userName + "'s Profile":"Translator"}</button>
            <button id="logout" onClick={() => logOut()}>Logout</button>
        </>
    )
}


function NotLoggedInHeader() {
    return (
        <>
            <img className="smallLogo"
            src={process.env.PUBLIC_URL + 'images/Logo.png'} 
            alt="Happy robot logo"/>
            <h2>Lost in Translation</h2>
        </>
    )
}

function Header() {
    const userName = useSelector(state => state.user);

    const isLoggedIn = useMemo(() => userName !== null, [userName]);

    return (
        <header>
            {isLoggedIn
                ? <LoggedInHeader userName={userName}/>
                : <NotLoggedInHeader/>
            }
        </header>
    )
}

export default Header;