import React, {useState, useEffect} from "react";
import useUser from "../hooks/useUser";
import { useHistory } from "react-router-dom";

function StartupPage() {
    const [name, setName] = useState("");
    const { isLoggedIn, logIn } = useUser();
    const history = useHistory();

    useEffect(() => {
        if (isLoggedIn) {
            history.push('/translator');
        }
        //eslint-disable-next-line
    }, [isLoggedIn])

    function onNamed(e) {
        setName(e.target.value.trim());
    }

    const handleLogin = () => {
        if (name !== "") {
            logIn(name);
        } else {
            alert("No name typed in!")
        }
    }
    

    function onKeyUp(e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            handleLogin();       
        }
    }
    
    //<PageLogo size="large"/>
    return (
        <section>
            <div>
                <img src={process.env.PUBLIC_URL + 'images/Logo-Hello.png'} alt="Hello logo" id="hello"/>
                <h3>Get Started!</h3>
            </div>
            <div id="welcome">   
                <input type="text" value={name} placeholder="What is your name?"
                    onChange={onNamed} onKeyUp={onKeyUp} id="nameInput">
                </input>
                <button id="loginButt" onClick={handleLogin}>Log In</button>
            </div>
        </section>  
    );
}

export default StartupPage;