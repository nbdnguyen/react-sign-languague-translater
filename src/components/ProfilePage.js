import React from "react";
import { useSelector } from "react-redux";
import useRedirect from "../hooks/useRedirect";
import {getStorage} from "../utils/storage";


function ProfilePage() {
    const userName = useSelector(state => state.user);
    const {history} = getStorage("history");
    useRedirect();

    function DisplayLog() {
        if(!history) {
            return(
            <p>Nothing in search history yet!</p>
            )
        } else {
            return (
                <div id="loglist">
                    {history.map((log, index) => 
                        <p key={index}>{log}</p> 
                    )}
                </div>
            )
        }
    }
    
    return (
        <section>
            <div>
                <h1>This is {userName}'s Profile page.</h1>
            </div>
            <div>
                <h4>Recent search history:</h4>
                <DisplayLog/>
            </div>
        </section>
    );
}

export default ProfilePage;